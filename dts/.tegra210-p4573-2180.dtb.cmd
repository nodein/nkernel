cmd_arch/arm64/boot/dts/tegra210-p4573-2180.dtb := gcc -E -Wp,-MD,arch/arm64/boot/dts/.tegra210-p4573-2180.dtb.d.pre.tmp -nostdinc -I/usr/src/kernel/arch/arm64/boot/dts -I/usr/src/kernel/arch/arm64/boot/dts/include -I/usr/src/kernel/arch/arm/boot/dts -undef -D__DTS__ -x assembler-with-cpp -o arch/arm64/boot/dts/.tegra210-p4573-2180.dtb.dts arch/arm64/boot/dts/tegra210-p4573-2180.dts ; /usr/src/kernel/scripts/dtc/dtc -O dtb -o arch/arm64/boot/dts/tegra210-p4573-2180.dtb -b 0 -i arch/arm64/boot/dts/ -i /usr/src/kernel/arch/arm/boot/dts -d arch/arm64/boot/dts/.tegra210-p4573-2180.dtb.d.dtc.tmp arch/arm64/boot/dts/.tegra210-p4573-2180.dtb.dts ; cat arch/arm64/boot/dts/.tegra210-p4573-2180.dtb.d.pre.tmp arch/arm64/boot/dts/.tegra210-p4573-2180.dtb.d.dtc.tmp > arch/arm64/boot/dts/.tegra210-p4573-2180.dtb.d

source_arch/arm64/boot/dts/tegra210-p4573-2180.dtb := arch/arm64/boot/dts/tegra210-p4573-2180.dts

deps_arch/arm64/boot/dts/tegra210-p4573-2180.dtb := \
  arch/arm64/boot/dts/tegra210-jetson-cv-base-p2597-2180-a00.dts \
  arch/arm64/boot/dts/tegra210-common.dtsi \
  arch/arm64/boot/dts/tegra210-soc-shield.dtsi \
  arch/arm64/boot/dts/tegra210-soc-base.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/arm-gic.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/irq.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/memory/tegra-swgroup.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/thermal/thermal.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/tegra-gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/padctrl/tegra210-pads.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/ata/ahci-tegra.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/clk/tegra210-clk.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/soc/nvidia,tegra210-powergate.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/usb/tegra-usb.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-bthrot-cdev.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-prods.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-touch-e1937-1000-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/spi/rm31080a_platform.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-modem-common.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-audio.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-power-tree-p2597-2180-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-common-power-tree-p2530-0930-e03.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-e-pmic-p2530-0930-e03.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/mfd/max77620.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/regulator/regulator.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-e-power-fixed-p2530-0930-e03.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-power-dvfs-e2174-1101-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-power-dvfs.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-pinmux-p2597-2180-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/pinctrl/pinctrl-tegra.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-pinmux-drive-sdmmc-common.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-prods.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-comms-p2530-0930.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-camera-modules.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-camera-e3333-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-camera-e3333-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/media/camera.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/platform/t210/t210.h \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-camera-e3322-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-camera-e3322-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-camera-e3323-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-camera-e3323-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-camera-e3326-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-camera-e3326-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-pwm-fan-p2530-0930.dtsi \
  /usr/src/kernel/arch/arm/boot/dts/panel-a-wuxga-8-0.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/display/tegra-dc.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/display/tegra-panel.h \
  /usr/src/kernel/arch/arm/boot/dts/panel-a-edp-1080p-14-0.dtsi \
  /usr/src/kernel/arch/arm/boot/dts/panel-s-edp-uhdtv-15-6.dtsi \
  /usr/src/kernel/arch/arm/boot/dts/panel-s-wqxga-10-1.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-hdmi-e2190-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-sdhci.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-p2180-common.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal-nct72-p2530.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal-Tboard-Tdiode.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-clk-init.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal-fan-est-p2530-0930.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-keys-p2597-2180-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/input/input.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/iio/meter/ina3221x.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-foster-e-extcon-p2530-0930-e01.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-emc-p2180-1000-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-edp.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-sysedp.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal-nct72-p2530.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal-Tboard-Tdiode.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-powermon-p2180-1000-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-powermon-p2597.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cpufreq.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-overrides/tegra210-power-dvfs-override-ucm1c.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-super-module-e2614.dtsi \
  arch/arm64/boot/dts/tegra210-plugin-manager/tegra210-jetson-cv-eeprom-manager.dtsi \
  arch/arm64/boot/dts/tegra210-plugin-manager/tegra210-jetson-cv-plugin-manager.dtsi \
  arch/arm64/boot/dts/tegra210-plugin-manager/tegra210-jetson-cv-camera-plugin-manager.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-jetson-cv-shim-p2597-2180-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-p4573-2180-gpio.dtsi \

arch/arm64/boot/dts/tegra210-p4573-2180.dtb: $(deps_arch/arm64/boot/dts/tegra210-p4573-2180.dtb)

$(deps_arch/arm64/boot/dts/tegra210-p4573-2180.dtb):
