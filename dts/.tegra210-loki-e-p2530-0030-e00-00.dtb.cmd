cmd_arch/arm64/boot/dts/tegra210-loki-e-p2530-0030-e00-00.dtb := gcc -E -Wp,-MD,arch/arm64/boot/dts/.tegra210-loki-e-p2530-0030-e00-00.dtb.d.pre.tmp -nostdinc -I/usr/src/kernel/arch/arm64/boot/dts -I/usr/src/kernel/arch/arm64/boot/dts/include -I/usr/src/kernel/arch/arm/boot/dts -undef -D__DTS__ -x assembler-with-cpp -o arch/arm64/boot/dts/.tegra210-loki-e-p2530-0030-e00-00.dtb.dts arch/arm64/boot/dts/tegra210-loki-e-p2530-0030-e00-00.dts ; /usr/src/kernel/scripts/dtc/dtc -O dtb -o arch/arm64/boot/dts/tegra210-loki-e-p2530-0030-e00-00.dtb -b 0 -i arch/arm64/boot/dts/ -i /usr/src/kernel/arch/arm/boot/dts -d arch/arm64/boot/dts/.tegra210-loki-e-p2530-0030-e00-00.dtb.d.dtc.tmp arch/arm64/boot/dts/.tegra210-loki-e-p2530-0030-e00-00.dtb.dts ; cat arch/arm64/boot/dts/.tegra210-loki-e-p2530-0030-e00-00.dtb.d.pre.tmp arch/arm64/boot/dts/.tegra210-loki-e-p2530-0030-e00-00.dtb.d.dtc.tmp > arch/arm64/boot/dts/.tegra210-loki-e-p2530-0030-e00-00.dtb.d

source_arch/arm64/boot/dts/tegra210-loki-e-p2530-0030-e00-00.dtb := arch/arm64/boot/dts/tegra210-loki-e-p2530-0030-e00-00.dts

deps_arch/arm64/boot/dts/tegra210-loki-e-p2530-0030-e00-00.dtb := \
  arch/arm64/boot/dts/tegra210-loki-e-p2530-common.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/types.h \
  arch/arm64/boot/dts/tegra210-common.dtsi \
  arch/arm64/boot/dts/tegra210-soc-shield.dtsi \
  arch/arm64/boot/dts/tegra210-soc-base.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/arm-gic.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/irq.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/memory/tegra-swgroup.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/thermal/thermal.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/tegra-gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/padctrl/tegra210-pads.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/ata/ahci-tegra.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/clk/tegra210-clk.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/soc/nvidia,tegra210-powergate.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/usb/tegra-usb.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-bthrot-cdev.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-prods.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-touch-e1937-1000-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/spi/rm31080a_platform.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-modem-common.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-sdhci.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-p2530-common.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal-nct72-p2530.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal-Tboard-Tdiode.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-clk-init.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-prods.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-power-tree-p2530-0999-e00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-pmic-e2174-1101-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/mfd/max77620.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/regulator/regulator.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-fixed-e2174-1101-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-power-dvfs-e2174-1101-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-power-dvfs.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-gpio-p2530-0032-e00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-pinmux-drive-sdmmc-common.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/pinctrl/pinctrl-tegra.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-pinmux-p2530-0032-e00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-comms.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-pwm-fan.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-pwm-fan-p2530-0930.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-hdmi-e2190-1100-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/display/tegra-dc.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-sensor-p2530-0032-e00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-thermal-fan-est.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-camera-p2530-0032-e00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/media/camera.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-keys-p2530-0032-e00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/input/input.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-maxim-charger.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-powermon.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-edp.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-sysedp.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-audio.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-gamepad-reset.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-c2port.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-cpufreq.dtsi \
  /usr/src/kernel/arch/arm/boot/dts/panel-j-1440-810-5-8.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/display/tegra-panel.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-emc-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-loki-e-touch-p2530-01xx.dtsi \

arch/arm64/boot/dts/tegra210-loki-e-p2530-0030-e00-00.dtb: $(deps_arch/arm64/boot/dts/tegra210-loki-e-p2530-0030-e00-00.dtb)

$(deps_arch/arm64/boot/dts/tegra210-loki-e-p2530-0030-e00-00.dtb):
