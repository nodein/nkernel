cmd_arch/arm64/boot/dts/tegra210-hawkeye-p2290-3303-a01-00-base.dtb := gcc -E -Wp,-MD,arch/arm64/boot/dts/.tegra210-hawkeye-p2290-3303-a01-00-base.dtb.d.pre.tmp -nostdinc -I/usr/src/kernel/arch/arm64/boot/dts -I/usr/src/kernel/arch/arm64/boot/dts/include -I/usr/src/kernel/arch/arm/boot/dts -undef -D__DTS__ -x assembler-with-cpp -o arch/arm64/boot/dts/.tegra210-hawkeye-p2290-3303-a01-00-base.dtb.dts arch/arm64/boot/dts/tegra210-hawkeye-p2290-3303-a01-00-base.dts ; /usr/src/kernel/scripts/dtc/dtc -O dtb -o arch/arm64/boot/dts/tegra210-hawkeye-p2290-3303-a01-00-base.dtb -b 0 -i arch/arm64/boot/dts/ -i /usr/src/kernel/arch/arm/boot/dts -d arch/arm64/boot/dts/.tegra210-hawkeye-p2290-3303-a01-00-base.dtb.d.dtc.tmp arch/arm64/boot/dts/.tegra210-hawkeye-p2290-3303-a01-00-base.dtb.dts ; cat arch/arm64/boot/dts/.tegra210-hawkeye-p2290-3303-a01-00-base.dtb.d.pre.tmp arch/arm64/boot/dts/.tegra210-hawkeye-p2290-3303-a01-00-base.dtb.d.dtc.tmp > arch/arm64/boot/dts/.tegra210-hawkeye-p2290-3303-a01-00-base.dtb.d

source_arch/arm64/boot/dts/tegra210-hawkeye-p2290-3303-a01-00-base.dtb := arch/arm64/boot/dts/tegra210-hawkeye-p2290-3303-a01-00-base.dts

deps_arch/arm64/boot/dts/tegra210-hawkeye-p2290-3303-a01-00-base.dtb := \
  arch/arm64/boot/dts/tegra210-hawkeye-p2290-3302-a01-00-base.dts \
  arch/arm64/boot/dts/tegra210-hawkeye-p2290-a00-00-base.dts \
  arch/arm64/boot/dts/tegra210-hawkeye-p2290-common.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/types.h \
  arch/arm64/boot/dts/tegra210-soc-shield.dtsi \
  arch/arm64/boot/dts/tegra210-soc-base.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/arm-gic.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/irq.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/memory/tegra-swgroup.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/thermal/thermal.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/tegra-gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/padctrl/tegra210-pads.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/ata/ahci-tegra.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/clk/tegra210-clk.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/soc/nvidia,tegra210-powergate.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/usb/tegra-usb.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-bthrot-cdev.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-prods.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-pinmux-p2290-1100-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/pinctrl/pinctrl-tegra.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-pinmux-drive-sdmmc-common.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-pinmux-manual-p2290-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-power-tree-p2290-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-pmic-p2290-1100-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/mfd/max77620.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/regulator/regulator.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/iio/meter/ina3221x.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-fixed-p2290-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-power-dvfs-e2174-1101-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-power-dvfs.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-keys-e2220-1170-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/input/input.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-maxim-charger.dtsi \
  /usr/src/kernel/arch/arm/boot/dts/panel-s-wuxga-7-0.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/display/tegra-dc.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/display/tegra-panel.h \
  /usr/src/kernel/arch/arm/boot/dts/panel-s-wuxga-8-0.dtsi \
  /usr/src/kernel/arch/arm/boot/dts/panel-a-wuxga-8-0.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-emc-p2290-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-touch.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/spi/rm31080a_platform.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/spi/lr388k7_platform.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-sensor-p2290-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-camera-p2290-1100-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/media/camera.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-modem-p2290-1100-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/tegra210-platforms/tegra210-modem-common.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-sdhci.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-edp.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-audio.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-thermal-nct72-e2220-1170-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal-Tboard-Tdiode.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-therm-est.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-clk-init.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-hdmi-e2190-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-keys-p2290-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-powermon.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-e-prods.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-thermal-zone-p2290-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-cpufreq.dtsi \
  arch/arm64/boot/dts/tegra210-plugin-manager/tegra210-hawkeye-plugin-manager.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-hawkeye-comms.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-comms.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-sysedp-670d.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-sysedp.dtsi \

arch/arm64/boot/dts/tegra210-hawkeye-p2290-3303-a01-00-base.dtb: $(deps_arch/arm64/boot/dts/tegra210-hawkeye-p2290-3303-a01-00-base.dtb)

$(deps_arch/arm64/boot/dts/tegra210-hawkeye-p2290-3303-a01-00-base.dtb):
