cmd_arch/arm64/boot/dts/tegra210-ers-e2190-1100-a00-00.dtb := gcc -E -Wp,-MD,arch/arm64/boot/dts/.tegra210-ers-e2190-1100-a00-00.dtb.d.pre.tmp -nostdinc -I/usr/src/kernel/arch/arm64/boot/dts -I/usr/src/kernel/arch/arm64/boot/dts/include -I/usr/src/kernel/arch/arm/boot/dts -undef -D__DTS__ -x assembler-with-cpp -o arch/arm64/boot/dts/.tegra210-ers-e2190-1100-a00-00.dtb.dts arch/arm64/boot/dts/tegra210-ers-e2190-1100-a00-00.dts ; /usr/src/kernel/scripts/dtc/dtc -O dtb -o arch/arm64/boot/dts/tegra210-ers-e2190-1100-a00-00.dtb -b 0 -i arch/arm64/boot/dts/ -i /usr/src/kernel/arch/arm/boot/dts -d arch/arm64/boot/dts/.tegra210-ers-e2190-1100-a00-00.dtb.d.dtc.tmp arch/arm64/boot/dts/.tegra210-ers-e2190-1100-a00-00.dtb.dts ; cat arch/arm64/boot/dts/.tegra210-ers-e2190-1100-a00-00.dtb.d.pre.tmp arch/arm64/boot/dts/.tegra210-ers-e2190-1100-a00-00.dtb.d.dtc.tmp > arch/arm64/boot/dts/.tegra210-ers-e2190-1100-a00-00.dtb.d

source_arch/arm64/boot/dts/tegra210-ers-e2190-1100-a00-00.dtb := arch/arm64/boot/dts/tegra210-ers-e2190-1100-a00-00.dts

deps_arch/arm64/boot/dts/tegra210-ers-e2190-1100-a00-00.dtb := \
  arch/arm64/boot/dts/tegra210-common.dtsi \
  arch/arm64/boot/dts/tegra210-soc-shield.dtsi \
  arch/arm64/boot/dts/tegra210-soc-base.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/arm-gic.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/irq.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/memory/tegra-swgroup.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/thermal/thermal.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/tegra-gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/padctrl/tegra210-pads.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/ata/ahci-tegra.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/clk/tegra210-clk.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/soc/nvidia,tegra210-powergate.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/usb/tegra-usb.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-bthrot-cdev.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-prods.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-touch-e1937-1000-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/spi/rm31080a_platform.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-modem-common.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-extgpio-e2220-1170-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-power-tree-e2174-1101-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-pmic-e2174-1101-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/mfd/max77620.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/regulator/regulator.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-fixed-e2174-1101-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-power-dvfs-e2174-1101-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-power-dvfs.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-sensor-e2160-1000-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-gpio-e2190-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-pinmux-drive-sdmmc-common.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/pinctrl/pinctrl-tegra.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-pinmux-e2190-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-comms.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-sdhci.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-audio.dtsi \
  /usr/src/kernel/arch/arm/boot/dts/panel-a-wuxga-8-0.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/display/tegra-dc.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/display/tegra-panel.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-hdmi-e2190-1100-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-keys-e2220-1170-a00.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/input/input.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-edp.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-thermal-nct72-e2220-1170-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal-Tboard-Tdiode.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-clk-init.dtsi \

arch/arm64/boot/dts/tegra210-ers-e2190-1100-a00-00.dtb: $(deps_arch/arm64/boot/dts/tegra210-ers-e2190-1100-a00-00.dtb)

$(deps_arch/arm64/boot/dts/tegra210-ers-e2190-1100-a00-00.dtb):
