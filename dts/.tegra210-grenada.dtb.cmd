cmd_arch/arm64/boot/dts/tegra210-grenada.dtb := gcc -E -Wp,-MD,arch/arm64/boot/dts/.tegra210-grenada.dtb.d.pre.tmp -nostdinc -I/usr/src/kernel/arch/arm64/boot/dts -I/usr/src/kernel/arch/arm64/boot/dts/include -I/usr/src/kernel/arch/arm/boot/dts -undef -D__DTS__ -x assembler-with-cpp -o arch/arm64/boot/dts/.tegra210-grenada.dtb.dts arch/arm64/boot/dts/tegra210-grenada.dts ; /usr/src/kernel/scripts/dtc/dtc -O dtb -o arch/arm64/boot/dts/tegra210-grenada.dtb -b 0 -i arch/arm64/boot/dts/ -i /usr/src/kernel/arch/arm/boot/dts -d arch/arm64/boot/dts/.tegra210-grenada.dtb.d.dtc.tmp arch/arm64/boot/dts/.tegra210-grenada.dtb.dts ; cat arch/arm64/boot/dts/.tegra210-grenada.dtb.d.pre.tmp arch/arm64/boot/dts/.tegra210-grenada.dtb.d.dtc.tmp > arch/arm64/boot/dts/.tegra210-grenada.dtb.d

source_arch/arm64/boot/dts/tegra210-grenada.dtb := arch/arm64/boot/dts/tegra210-grenada.dts

deps_arch/arm64/boot/dts/tegra210-grenada.dtb := \
  arch/arm64/boot/dts/tegra210-soc-shield.dtsi \
  arch/arm64/boot/dts/tegra210-soc-base.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/arm-gic.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/irq.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/memory/tegra-swgroup.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-thermal.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/thermal/thermal.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/tegra-gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/gpio/gpio.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/padctrl/tegra210-pads.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/ata/ahci-tegra.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/clk/tegra210-clk.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/soc/nvidia,tegra210-powergate.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/usb/tegra-usb.h \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-bthrot-cdev.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-grenada-fixed.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-ers-power-dvfs-e2174-1101-a00.dtsi \
  arch/arm64/boot/dts/tegra210-platforms/tegra210-power-dvfs.dtsi \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/pinctrl/pinctrl-tegra.h \
  /usr/src/kernel/arch/arm64/boot/dts/include/dt-bindings/sound/tegra-asoc-alt.h \

arch/arm64/boot/dts/tegra210-grenada.dtb: $(deps_arch/arm64/boot/dts/tegra210-grenada.dtb)

$(deps_arch/arm64/boot/dts/tegra210-grenada.dtb):
